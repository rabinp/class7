describe("Soccer Game Functions", () => {
    describe("getTotalPoints", () => {
        it ("should return a score of 9 for 3 wins", () =>{
            const result = getTotalPoints('www');
            expect(result).toBe(9)
        })
    })

    describe("orderTeam", () =>{
        it("should return a string of teams output in order", () => {
          
            const result = orderTeams(
                { name: "Sounders", results: "wwdl" },
                { name: "Galaxy", results: "wlld" }
            );
            expect(result).toBe('Sounders: 7 \nGalaxy: 4')
        });
    });
});