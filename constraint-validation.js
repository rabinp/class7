// TODO

const firstName = document.getElementById("first-name")
firstName.addEventListener('input', function(){
    console.log("Frist Name Changed!")
    console.log(this.value)
})

const lastName = document.getElementById("last-name")
lastName.addEventListener('change', function() {
    console.log("Last Name Changed!")
    console.log(this.validity)
})

const email = document.getElementById("email")
email.addEventListener('change', function () {

    console.log("Email Changed!")
    const emailString = this.value

    if (emailString.length < 5) {
        this.setCustomValidity('Please enter a longer email address')
        this.reportValidity()
    }
})

